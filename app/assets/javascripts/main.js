var shoppingCart = (function() {
  // =============================
  // Private methods and propeties
  // =============================
  cart = [];

  // Constructor
  function Item(image, name, price, count) {
    this.image = image;
    this.name = name;
    this.price = price;
    this.count = count;
  }

  // Save cart
  function saveCart() {
    localStorage.setItem("carts", JSON.stringify(cart));
  }

  // Load cart
  function loadCart() {
    cart = JSON.parse(localStorage.getItem("carts"));
  }
  if (localStorage.getItem("carts") != null) {
    loadCart();
  }

  // =============================
  // Public methods and propeties
  // =============================
  var obj = {};

  // Add to cart
  obj.addItemToCart = function(image, name, price, count) {
    for (var item in cart) {
      if (cart[item].name === name) {
        cart[item].count++;
        saveCart();
        return;
      }
    }
    var item = new Item(image, name, price, count);
    cart.push(item);
    saveCart();
  };
  // Set count from item
  obj.setCountForItem = function(name, count) {
    for (var i in cart) {
      if (cart[i].name === name) {
        cart[i].count = count;
        break;
      }
    }
  };
  // Remove item from cart
  obj.removeItemFromCart = function(name) {
    for (var item in cart) {
      if (cart[item].name === name) {
        cart[item].count--;
        if (cart[item].count === 0) {
          cart.splice(item, 1);
        }
        break;
      }
    }
    saveCart();
  };

  // Remove all items from cart
  obj.removeItemFromCartAll = function(name) {
    for (var item in cart) {
      if (cart[item].name === name) {
        cart.splice(item, 1);
        break;
      }
    }
    saveCart();
  };

  // Clear cart
  obj.clearCart = function() {
    cart = [];
    saveCart();
  };

  // Count cart
  obj.totalCount = function() {
    var totalCount = 0;
    for (var item in cart) {
      totalCount += cart[item].count;
    }
    return totalCount;
  };

  // Total cart
  obj.totalCart = function() {
    var totalCart = 0;
    for (var item in cart) {
      totalCart += cart[item].price * cart[item].count;
    }
    return Number(totalCart);
  };

  // List cart
  obj.listCart = function() {
    var cartCopy = [];
    for (i in cart) {
      item = cart[i];
      itemCopy = {};
      for (p in item) {
        itemCopy[p] = item[p];
      }
      itemCopy.total = Number(item.price * item.count);
      cartCopy.push(itemCopy);
    }
    return cartCopy;
  };

  // cart : Array
  // Item : Object/Class
  // addItemToCart : Function
  // removeItemFromCart : Function
  // removeItemFromCartAll : Function
  // clearCart : Function
  // countCart : Function
  // totalCart : Function
  // listCart : Function
  // saveCart : Function
  // loadCart : Function
  return obj;
})();

function displayCart() {
  var cartArray = shoppingCart.listCart();
  var output = "";
  var cartThumbnailOutPut = "";
  for (var i in cartArray) {
    output +=
      "<tr>" +
      "<td class='product-thumbnail'><img src='" +
      cartArray[i].image +
      "' alt='image product'></td>" +
      "<td class='product-name'><h2 class='h5 text-black'>" +
      cartArray[i].name +
      "</h2></td>" +
      "<td>" +
      cartArray[i].price +
      " VND</td>" +
      "<td><div class='input-group mb-3 m-auto input-quality'><div class='input-group-prepend'><button class='btn btn-outline-primary js-btn-minus' data-name=" +
      cartArray[i].name +
      " data-image=" +
      cartArray[i].image +
      ">-</button></div>" +
      "<input type='text' class='text-center form-control' data-name='" +
      cartArray[i].name +
      "' value='" +
      cartArray[i].count +
      "' disabled>" +
      "<div class='input-group-append'><button class='btn btn-outline-primary js-btn-plus' data-name=" +
      cartArray[i].name +
      " data-image=" +
      cartArray[i].image +
      ">+</button></div></div></td>" +
      "<td>" +
      cartArray[i].total +
      " VND</td>" +
      "<td><button class='btn btn-primary height-auto btn-sm delete-item' data-name=" +
      cartArray[i].name +
      ">X</button></td>" +
      "</tr>";

    cartThumbnailOutPut +=
      "<li class='clearfix col-12'>" +
      "<img src='" +
      cartArray[i].image +
      "' alt='cart image' width='80px' />" +
      "<span class='item-name'>" +
      cartArray[i].name +
      "</span><span>Số lượng: " +
      cartArray[i].count +
      "</span></li>";
  }
  $(".show-cart").html(output);
  $(".shopping-cart-items").html(cartThumbnailOutPut);
  $(".total-cart").html(shoppingCart.totalCart() + " VND");
  $(".total-count").html(shoppingCart.totalCount());
}

$(function() {
  $(window).on("load", function() {
    displayCart();

    $("#loader-wrapper").fadeOut(500);

    if ($.fn.classyNav) {
      $("#akameNav").classyNav();
    }

    $(document).on("click", "#cart", function() {
      $(".shopping-cart").fadeToggle("fast");
    });

    $("#message").fadeOut(10000);

    /******************************************
	Youtube Video slider
******************************************/
    $(".fp-slider-items").slick({
      dots: true,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    $(".popup-youtube").magnificPopup({
      type: "iframe",
      mainClass: "mfp-fade",
      preloader: true
    });

    /******************************************
	Project slick at homepage
******************************************/
    $(".project").slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    });

    /******************************************
	Homepage slider
******************************************/
    $("#slidy").bxSlider({
      auto: true,
      pager: false
    });

    /******************************************
	Image slider in product detail
******************************************/

    var $imagesSlider = $(".gallery-slider .gallery-slider__images>div"),
      $thumbnailsSlider = $(".gallery-slider__thumbnails>div");

    // images options
    $imagesSlider.slick({
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: "linear",
      fade: true,
      draggable: false,
      prevArrow: ".gallery-slider__images .prev-arrow",
      nextArrow: ".gallery-slider__images .next-arrow"
    });

    // thumbnails options
    $thumbnailsSlider.slick({
      speed: 300,
      slidesToShow: 5,
      slidesToScroll: 1,
      cssEase: "linear",
      centerMode: true,
      draggable: false,
      focusOnSelect: true,
      asNavFor: ".gallery-slider .gallery-slider__images>div",
      prevArrow: ".gallery-slider__thumbnails .prev-arrow",
      nextArrow: ".gallery-slider__thumbnails .next-arrow",
      responsive: [
        {
          breakpoint: 720,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 350,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    });

    /******************************************
	Image responsive
******************************************/
    $(".function img").addClass("img-fluid");
    $(".blog-detail img").addClass("img-fluid");

    /******************************************
	cart
******************************************/
    $(document).on("click", ".add-cart-btn", function(e) {
      e.preventDefault();
      var name = $(this).data("name");
      var price = Number($(this).data("price"));
      var image = $(this).data("image");
      shoppingCart.addItemToCart(image, name, price, 1);

      $(".notice")
        .html(
          "<div class='alert alert-success alert-dismissable'> <button type='button' class='close' data-dismiss='alert'  aria-hidden='true'>&times;</button> Đã thêm vào giỏ hàng</div>"
        )
        .css("display", "")
        .delay(1500)
        .fadeOut(400);
      displayCart();
    });

    $(".show-cart").on("click", ".delete-item", function(e) {
      var name = $(this).data("name");
      shoppingCart.removeItemFromCartAll(name);
      displayCart();
    });
    $(".show-cart").on("click", ".js-btn-minus", function(event) {
      var name = $(this).data("name");
      shoppingCart.removeItemFromCart(name);
      displayCart();
    });
    // +1
    $(".show-cart").on("click", ".js-btn-plus", function(event) {
      var name = $(this).data("name");
      var image = $(this).data("image");
      shoppingCart.addItemToCart(image, name);
      displayCart();
    });
    // clear cart
    $(document).on("click", ".clear-cart", function() {
      shoppingCart.clearCart();
      displayCart();
    });

    // order send
    $(document).on("click", "#order-btn", function() {
      var carts = shoppingCart.listCart();
      var name = $("#name").val();
      var phone = $("#phone").val();
      var address = $("#address").val();

      if (name || phone || address) {
        $.ajax({
          method: "PATCH",
          url: "/order",
          dataType: "json",
          data: {
            payload: {
              carts: carts,
              name: name,
              phone: phone,
              address: address
            }
          }
        });
        window.location = "/thankyou";
        localStorage.removeItem("carts");
      } else {
        $(".notice")
          .html(
            "<div class='alert alert-success alert-dismissable'> <button type='button' class='close' data-dismiss='alert'  aria-hidden='true'>&times;</button> Hãy nhập đầy đủ thông tin</div>"
          )
          .css("display", "")
          .delay(1500)
          .fadeOut(400);
      }
    });
  });
});
