//= require jquery
//= require jquery_ujs
//= require popper
//= require bootstrap
//= require tinymce
//= require moment
//= require moment-timezone-with-data
//= require tempusdominus-bootstrap-4

$(function() {
  $("#datetimepicker1").datetimepicker({
    format: "L"
  });

  $("#message").fadeOut(10000);
});
