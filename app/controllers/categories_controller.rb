class CategoriesController < ApplicationController
    before_action :set_category, only: [:show]

    def show
        @categories = Category.all
        @products_of_category = @category.products.paginate(:page => params[:page], :per_page => 10)
        @new_products = Product.order(created_at: :desc).take 3
    end

    private
    def set_category
        @category = Category.find params[:id]
    end
end