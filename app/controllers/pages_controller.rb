class PagesController < ApplicationController
  def index
    @tab_categories = Array.new
    Category.all.each do |category| 
      if (category.products.length > 0) 
        @tab_categories << category
      end
    end
    @videos = Video.order(created_at: :desc)
    @banners = Banner.all
    @blogs = Blog.all.take(15)
  end
  
  def about
  end

  def care
  end

  def cart
  end

  def order
    OrderMailer.with(payload: params[:payload]).new_order_email.deliver
  end

  def thankyou
    render layout: 'special'
  end
end