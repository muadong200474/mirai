class Admin::BannersController < Admin::AdminController
  before_action :set_banner, only: [:destroy]
  
  def index
      @banners = Banner.paginate(:page => params[:page], :per_page => 10)
  end

  def new 
      @banner = Banner.new
  end

  def create
      @banner = Banner.new banner_params
      if @banner.save
          flash[:success] = "Created successfully!!"
          redirect_to admin_banners_path
      else
          render :new
      end
  end

  def destroy
      delete_image = @banner.bannerimage
      if @banner.destroy
          flash[:success] = "Deleted!!"
          delete_image.try(:remove!) #delete at s3
          redirect_to admin_banners_path
      end
  end

  private
  def banner_params
      params.require(:banner).permit(:bannerimage)
  end

  def set_banner
      @banner = Banner.find params[:id]
  end
end
