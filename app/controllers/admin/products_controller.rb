class Admin::ProductsController < Admin::AdminController
    before_action :set_product, only: [:edit, :update, :show, :destroy]
    def index
      if params[:cate_id].to_i != 0
        @category = Category.find params[:cate_id]
        @products = @category.products.paginate(:page => params[:page], :per_page => 10)
      else
        @products = Product.paginate(:page => params[:page], :per_page => 10)
      end
    end

    def new
      @product = Product.new
    end
  
    def create
      @product = Product.new product_params
      if product_params[:images]
        add_more_images(product_params[:images])
      end
      if @product.save
        flash[:success] = "Added!"
        redirect_to admin_products_path
      else
        render :new
      end
    end

    def edit
    end
  
    def update
      if product_params[:images]
        @product.images = []
      end
      if @product.update_attributes product_params
        flash[:success] = "Updated!!"
        redirect_to admin_products_path
      else
        render :edit
      end
    end
  
    def destroy
      if @product.destroy
        flash[:success] = "Deleted!"
        redirect_to admin_products_path
      end
    end

    private
    def product_params
      params.require(:product).permit(:name, :description, :price, {images: []}, :category_id, :information, :sale_off, :expired_date)
    end

    def set_product
      @product = Product.find params[:id]
    end

    def add_more_images(new_images)
      images = @product.images
      images += new_images
      @product.images = images
    end
  end