class Admin::BlogsController < Admin::AdminController
  before_action :set_blog, only: [:edit, :update, :show, :destroy]
  
  def index
      @blogs = Blog.paginate(:page => params[:page], :per_page => 10)
  end

  def new 
      @blog = Blog.new
  end

  def create
      @blog = Blog.new blog_params
      if @blog.save
          flash[:success] = "Created successfully!!"
          redirect_to admin_blogs_path
      else
          render :new
      end
  end

  def edit
  end

  def update
      if @blog.update_attributes blog_params
          flash[:success] = "Updated!!"
          redirect_to admin_blogs_path
      else
          render :edit
      end
  end

  def destroy
      if @blog.destroy
          flash[:success] = "Deleted!!"
          redirect_to admin_blogs_path
      end
  end

  private
  def blog_params
      params.require(:blog).permit(:title, :desc, :content, :image)
  end

  def set_blog
      @blog = Blog.find params[:id]
  end
end
