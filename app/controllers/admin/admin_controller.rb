class Admin::AdminController < ActionController::Base
    layout 'admin'
    protect_from_forgery with: :exception
    before_action :authenticate_user!, :check_authorized

    def index
    end

    private
    def check_authorized
        redirect_to root_path unless can? :manage, :all
    end
end