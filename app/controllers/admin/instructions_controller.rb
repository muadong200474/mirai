class Admin::InstructionsController < Admin::AdminController
  before_action :set_instruction, only: [:edit, :update, :show, :destroy]
  
  def index
      @instructions = Instruction.paginate(:page => params[:page], :per_page => 10)
  end

  def new 
      @instruction = Instruction.new
  end

  def create
      @instruction = Instruction.new instruction_params
      if @instruction.save
          flash[:success] = "Created successfully!!"
          redirect_to admin_instructions_path
      else
          render :new
      end
  end

  def edit
  end

  def update
      if @instruction.update_attributes instruction_params
          flash[:success] = "Updated!!"
          redirect_to admin_instructions_path
      else
          render :edit
      end
  end

  def destroy
      if @instruction.destroy
          flash[:success] = "Deleted!!"
          redirect_to admin_instructions_path
      end
  end

  private
  def instruction_params
      params.require(:instruction).permit(:title, :image, :content)
  end

  def set_instruction
      @instruction = Instruction.find params[:id]
  end
end
