class Admin::CategoriesController < Admin::AdminController
    before_action :set_category, only: [:edit, :update, :show, :destroy]
    
    def index
        @categories = Category.paginate(:page => params[:page], :per_page => 10)
    end

    def new 
        @category = Category.new
    end

    def create
        @category = Category.new category_params
        if @category.save
            flash[:success] = "Created successfully!!"
            redirect_to admin_categories_path
        else
            render :new
        end
    end

    def edit
    end

    def update
        if @category.update_attributes category_params
            flash[:success] = "Updated!!"
            redirect_to admin_categories_path
        else
            render :edit
        end
    end

    def destroy
        if @category.destroy
            flash[:success] = "Deleted!!"
            redirect_to admin_categories_path
        end
    end

    private
    def category_params
        params.require(:category).permit(:title, :homepage_banner)
    end

    def set_category
        @category = Category.find params[:id]
    end
end
