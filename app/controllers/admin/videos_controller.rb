class Admin::VideosController < Admin::AdminController
  before_action :set_video, only: [:edit, :update, :destroy]
  
  def index
      @videos = Video.paginate(:page => params[:page], :per_page => 10)
  end

  def new 
      @video = Video.new
  end

  def create
      @video = Video.new video_params
      if @video.save
          flash[:success] = "Created successfully!!"
          redirect_to admin_videos_path
      else
          render :new
      end
  end

  def edit
  end

  def update
    if @video.update_attributes video_params
      flash[:success] = "Updated!!"
      redirect_to admin_videos_path
    else
      render :edit
    end
  end

  def destroy
      if @video.destroy
          flash[:success] = "Deleted!!"
          redirect_to admin_videos_path
      end
  end

  private
  def video_params
      params.require(:video).permit(:youtube_link, :image)
  end

  def set_video
      @video = Video.find params[:id]
  end
end
