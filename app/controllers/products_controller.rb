class ProductsController < ApplicationController
    def show
        @product = Product.find params[:id]
        @other_products = Product.order("RANDOM()").take 6
        @blogs = Blog.all.take(15)
    end
end
