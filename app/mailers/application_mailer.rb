class ApplicationMailer < ActionMailer::Base
  default from: 'noreply200474@gmail.com'
  layout 'mailer'
end
