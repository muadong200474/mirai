class OrderMailer < ApplicationMailer
  def new_order_email
    @payload = params[:payload]

    mail(to: "sonpt91.bkhn@gmail.com", subject: "You got a new order!!")
  end
end