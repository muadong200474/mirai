class Category < ApplicationRecord
    extend FriendlyId
    friendly_id :title, use: [:slugged, :finders]
    mount_uploader :homepage_banner, ImageUploader

    has_many :products, dependent: :destroy

    def count_by_id(id)
        @category = Category.find id
        @category.products.count
    end

    def get_some_products(category_id, number = 4)
        @category = Category.find category_id
        @category.products.take number
    end
end