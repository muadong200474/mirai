class Instruction < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  mount_uploader :image, ImageUploader

  def previous_post
    self.class.where("id < ?", id).order("id desc").first
  end
  
  def next_post
    self.class.where("id > ?", id).order("id asc").first
  end
end
