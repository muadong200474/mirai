class Product < ApplicationRecord
    extend FriendlyId
    friendly_id :name, use: [:slugged, :finders]

    belongs_to :category, foreign_key: "category_id"

    mount_uploaders :images, ImageUploader

    def get_saleoff_price
        if self.sale_off > 0
            self.sale_off
        else
            self.price
        end
    end
end
