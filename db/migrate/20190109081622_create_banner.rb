class CreateBanner < ActiveRecord::Migration[5.2]
  def change
    create_table :banners do |t|
      t.string :bannerimage

      t.timestamps
    end
  end
end
