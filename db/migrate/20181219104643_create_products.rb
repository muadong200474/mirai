class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.string :information
      t.integer :price
      t.string :images, array: true, default: []
      t.references :category, foreign_key: true
      
      t.timestamps
    end
  end
end
