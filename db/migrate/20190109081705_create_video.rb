class CreateVideo < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |t|
      t.string :youtube_id
      t.string :youtube_link

      t.timestamps
    end
  end
end
