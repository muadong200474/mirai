class AddHomepageBannerToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :homepage_banner, :string
  end
end
