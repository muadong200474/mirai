class CreateInstructions < ActiveRecord::Migration[5.2]
  def change
    create_table :instructions do |t|
      t.string :title
      t.string :content
      t.string :image

      t.timestamps
    end
  end
end
