class AddSaleToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :sale_off, :integer, default: 0
    add_column :products, :expired_date, :datetime
  end
end
