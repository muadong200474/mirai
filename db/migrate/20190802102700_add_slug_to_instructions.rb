class AddSlugToInstructions < ActiveRecord::Migration[5.2]
  def change
    add_column :instructions, :slug, :string
    add_index :instructions, :slug, unique: true
  end
end
