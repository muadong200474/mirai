Rails.application.routes.draw do

  devise_for :users
  root to: "pages#index"
  get "/about", to: "pages#about"
  get "/cart", to: "pages#cart"
  get "/care", to: "pages#care"
  patch "/order", to: "pages#order"
  get "/thankyou", to: "pages#thankyou"
  resources :categories
  resources :products
  resources :blogs
  resources :instructions

  # admin side
  namespace :admin do
    root to: "admin#index"
    resources :products
    resources :categories
    resources :users, only: [:index]
    resources :banners
    resources :videos
    resources :blogs
    resources :instructions
  end
  
end
